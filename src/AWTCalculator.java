import java.awt.*;
import java.awt.event.*;

public class AWTCalculator extends Frame implements ActionListener {

    private Frame mainFrame;
    private TextField tfDisplay;
    private Panel btnPanel;
    private int curVal;
    private String[] btnNameArr =
            {"1/x", "x!", "C", "CE", "B",
                    "7", "8", "9", "/", "X",
                    "4", "5", "6", "x^2","-",
                    "1", "2", "3", "x^(1/2)", "+",
            "0", ".", "+/-", "%", "="};


    public AWTCalculator () {
        mainFrame = new Frame("Calculator");
        mainFrame.setSize(250, 300);
        mainFrame.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        tfDisplay = new TextField(curVal + "", 26);
        tfDisplay.setEditable(false);
        c.fill = GridBagConstraints.CENTER;
        c.gridx = c.gridy = 0;
        c.ipadx = c.ipady = 0;
        c.gridwidth = c.gridheight = 1;
        c.weightx = c.weighty = 0.5;
        mainFrame.add(tfDisplay, c);

        btnPanel = new Panel(new GridBagLayout());
        btnPanel.setSize(100, 100);
        GridBagConstraints d = new GridBagConstraints();
        d.ipadx = d.ipady = 0;
        d.gridwidth = 1;
        d.gridheight = 1;
        d.weightx = d.weighty = 0.5;
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                Button btn = new Button(btnNameArr[5*i+j]);
                btn.setPreferredSize(new Dimension(40, 40));
                d.gridx = j;
                d.gridy = i;

                btn.addActionListener(
                        new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {

                            }
                        }
                );

                btnPanel.add(btn, d);
            }

        }
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.PAGE_START;
        c.gridx = 0;
        c.gridy = 1;
        c.ipadx = c.ipady = 5;
        c.gridwidth = c.gridheight = 1;
        c.weightx = c.weighty = 0.1;
        mainFrame.add(btnPanel, c);
        mainFrame.setVisible(true);
    }

    public static void main(String[] args) {
        AWTCalculator app = new AWTCalculator();
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
