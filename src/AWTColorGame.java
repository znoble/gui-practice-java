import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class AWTColorGame extends Frame implements ActionListener {

    Random rand = new Random();
    private Frame mainFrame;
    private Label lblColor;
    private Button btnColor;

    private Map<String, Color> ColorMap = new HashMap<>();
    private String[] colors_arr = {"RED", "BLUE", "YELLOW", "GREEN", "PINK", "ORANGE", "BLACK", "WHITE"};

    public AWTColorGame () {

        ColorMap.put("RED", Color.RED);
        ColorMap.put("GREEN", Color.GREEN);
        ColorMap.put("BLUE", Color.BLUE);
        ColorMap.put("YELLOW", Color.YELLOW);
        ColorMap.put("PINK", Color.PINK);
        ColorMap.put("ORANGE", Color.ORANGE);
        ColorMap.put("BLACK", Color.BLACK);
        ColorMap.put("WHITE", Color.WHITE);

        mainFrame = new Frame("Color Game");
        mainFrame.setSize(250, 100);
        mainFrame.setLayout(new GridLayout(1, 2));
        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        lblColor = new Label();
        lblColor.setText(colors_arr[rand.nextInt(colors_arr.length)]);
        lblColor.setForeground(ColorMap.get(colors_arr[rand.nextInt(colors_arr.length)]));
        lblColor.setBackground(Color.DARK_GRAY);

        btnColor = new Button("Next Color");
        btnColor.addActionListener(this);

        mainFrame.add(lblColor);
        mainFrame.add(btnColor);
        mainFrame.setVisible(true);

    }

    public static void main(String[] args) {
        AWTColorGame app = new AWTColorGame();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        lblColor.setText(colors_arr[rand.nextInt(colors_arr.length)]);
        lblColor.setForeground(ColorMap.get(colors_arr[rand.nextInt(colors_arr.length)]));
    }
}
